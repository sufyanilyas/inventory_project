<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function index()
	{
		$this->load->view('common/header');
		$this->load->view('common/body_start');
		$this->load->view('common/wrapper_start');
		$this->load->view('common/sidebar');
		$this->load->view('common/main_panel_start');
		$this->load->view('common/navbar');
		$this->load->view('pages/dashboard/dashboard');
		$this->load->view('common/footer');
		$this->load->view('common/main_panel_end');
		$this->load->view('common/wrapper_end');
		$this->load->view('common/theme_settings');
		$this->load->view('common/core_js_files');
		$this->load->view('pages/dashboard/dashboard_js');
		$this->load->view('common/body_end');
	}
}
